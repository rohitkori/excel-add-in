import * as React from "react";
import { Button, ButtonType } from "office-ui-fabric-react";
import Header from "./Header";
import HeroList, { HeroListItem } from "./HeroList";
import Selection from "./Selection";
import Progress from "./Progress";
import {columnMap} from './ColumnMap';
/* global Button, console, Excel, Header, HeroList, HeroListItem, Progress */

export interface AppProps {
  title: string;
  isOfficeInitialized: boolean;
}

export interface AppState {
  listItems: HeroListItem[];
  headerValues: string[];
  selectedHeaderIndexes: number[];
  selectedRows: string;
}

let lastRow = 1000;

function drawChart(context, selectedRows: string, selectedHeaderIndexes: number[]) {
  var currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
  let rowA = selectedRows.split(':');
  let rowNumbers = [rowA[0].split('!')[1]];
  let rows : Excel.Range, headers: Excel.Range, columnNo = 2;

  rowNumbers.forEach((row)=> {
    rows = currentWorksheet.getRange(`A${lastRow}`).copyFrom(`A${row}`);
    selectedHeaderIndexes.forEach(index => {
      rows = currentWorksheet.getRange(`${columnMap[columnNo]}${lastRow}`);
      headers = currentWorksheet.getRange(`${columnMap[columnNo]}${lastRow-1}`);
      let rowAddress =`${columnMap[index+1]}${row}`;
      let headerAddress = `${columnMap[index+1]}2`;
      rows.copyFrom(rowAddress);
      headers.copyFrom(headerAddress);
      columnNo++;
    });
  });

  rows = currentWorksheet.getRange(`A${lastRow}`).getEntireRow().getUsedRange();
  headers = currentWorksheet.getRange(`A${lastRow-1}`).getEntireRow().getUsedRange();
  return {
    rows,
    headers
  }
}

export default class App extends React.Component<AppProps, AppState> {
  constructor(props, context) {
    super(props, context);
    this.state = {
      listItems: [],
      headerValues: [],
      selectedHeaderIndexes: [],
      selectedRows: ''
    };
  }

  componentDidMount() {
    this.setState({
      listItems: [
        {
          icon: "Design",
          primaryText: "Create and visualize like a pro"
        }
      ]
    });
  }

  click = async () => {
    try {
      await Excel.run(async context => {
        /**
         * Insert your Excel code here
         */
          var currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
          var dataRange = context.workbook.getSelectedRange();
          var rows = dataRange.getEntireRow();
          var headers = currentWorksheet.getRange("A2").getEntireRow().getUsedRange();
          headers.load("values");
          rows.load("address");

          await context.sync().then(()=>{
            this.setState({headerValues: headers.values[0], selectedRows: rows.address});
            console.log(rows.address);
          })
      });
    } catch (error) {
      console.error(error);
    }
  };

  createChart = async () => {
    try {
      await Excel.run(async context => {
        /**
         * Insert your Excel code here
         */
          var currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
          const {rows, headers} = drawChart(context, this.state.selectedRows, this.state.selectedHeaderIndexes);
          var chart = currentWorksheet.charts.add('ColumnClustered', rows , 'Rows');
          chart.setPosition("B5", "I30");
          chart.title.text = "AlphaSense";
          chart.legend.position = "Right";
          chart.series.getItemAt(0).setXAxisValues(headers);
          chart.legend.format.fill.setSolidColor("white");
          chart.dataLabels.format.font.size = 15;
          chart.dataLabels.format.font.color = "black";

          chart.onDeactivated.add(async (event)=> {
            await Excel.run(async context => {
              var currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
              let rows = currentWorksheet.getRange(`A${lastRow}`).getEntireRow().getUsedRange();
              let headers = currentWorksheet.getRange(`A${lastRow-1}`).getEntireRow().getUsedRange();
              headers.delete("Left");
              rows.delete("Left");
              this.setState({
                headerValues: [],
                selectedHeaderIndexes: [],
                selectedRows: ''
              });
              await context.sync();
            });
        });
        await context.sync();
      });
    } catch (error) {
      console.error(error);
    }
  };

  toggle = (element: string) => {
    let {selectedHeaderIndexes, headerValues} = this.state;
    const rowIndex = headerValues.indexOf(element);
    const index = selectedHeaderIndexes.indexOf(rowIndex);
    if(index > -1) {
      selectedHeaderIndexes.splice(index,1);
    }
    else {
      selectedHeaderIndexes = [...selectedHeaderIndexes, rowIndex];
    }

    selectedHeaderIndexes.sort();
    this.setState({selectedHeaderIndexes: selectedHeaderIndexes});
  }

  render() {
    const { title, isOfficeInitialized } = this.props;

    if (!isOfficeInitialized) {
      return (
        <Progress title={title} logo="assets/logo-filled.png" message="Please sideload your addin to see app body." />
      );
    }

    return (
      <div className="ms-welcome">
        <Header logo="assets/logo-filled.png" title={this.props.title} message="Welcome" />
        {this.state.headerValues.length ?
        <div>
          <Selection
            quarterValues = {this.state.headerValues}
            onChange = {this.toggle}
            selections = {this.state.selectedHeaderIndexes}
          />
          <Button
              className="ms-welcome__action"
              buttonType={ButtonType.hero}
              iconProps={{ iconName: "ChevronRight" }}
              onClick={this.createChart}
            >
              Create Chart
          </Button>
        </div>
        :
        <HeroList message="Discover what Office Add-ins can do for you today!" items={this.state.listItems}>
          <Button
            className="ms-welcome__action"
            buttonType={ButtonType.hero}
            iconProps={{ iconName: "ChevronRight" }}
            onClick={this.click}
          >
            Create Chart
          </Button>
        </HeroList>
        }
      </div>
    );
  }
}
