import * as React from "react";

export interface SelectionProps {
  quarterValues: string[];
  onChange: (listElement: string, value: boolean ) => void;
  selections: number[];
}

export default class Selection extends React.Component<SelectionProps> {
  render() {
    let {quarterValues, selections} = this.props;
    quarterValues = quarterValues.filter(i => i!="");
    return (
      <div>
        <h4>Please select quarter values</h4>
        {quarterValues && quarterValues.map((item,index) => (
            <Checkbox 
              rowContent= {item}
              uniqueKey = {index.toString()}
              index= {index}
              checked= {selections.includes(index+1) ? true : false}
              onChange= {this.props.onChange}
              listElement= {item}
            ></Checkbox>
          ))
        }
      </div>  
    );
  }
}

export const Checkbox:  React.SFC<ICheckbox> = (props) => {
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    props.onChange(props.listElement, e.target.checked);
  };

  return (
    <li
      className="filter-list-item"
      data-index={props.index}
      key={props.uniqueKey}
    >
      <input
        onChange={onChange}
        id={props.uniqueKey}
        type="checkbox"
        value="0"
        checked={props.checked}
      />
      {props.rowContent}
    </li>
  );
};

interface ICheckbox {
  index?: number;
  onChange: (listElement: string, checked: boolean) => void;
  listElement: string;
  checked: boolean;
  rowContent: string;
  uniqueKey?: string;
};